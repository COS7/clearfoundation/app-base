<?php

/**
 * Generic REST client.
 *
 * @category   apps
 * @package    base
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/base/
 */

///////////////////////////////////////////////////////////////////////////////
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// N A M E S P A C E
///////////////////////////////////////////////////////////////////////////////

namespace clearos\apps\base;

///////////////////////////////////////////////////////////////////////////////
// B O O T S T R A P
///////////////////////////////////////////////////////////////////////////////

$bootstrap = getenv('CLEAROS_BOOTSTRAP') ? getenv('CLEAROS_BOOTSTRAP') : '/usr/clearos/framework/shared';
require_once $bootstrap . '/bootstrap.php';

///////////////////////////////////////////////////////////////////////////////
// T R A N S L A T I O N S
///////////////////////////////////////////////////////////////////////////////

clearos_load_language('base');

///////////////////////////////////////////////////////////////////////////////
// D E P E N D E N C I E S
///////////////////////////////////////////////////////////////////////////////

// Classes
//--------

use \clearos\apps\base\Engine as Engine;
use \clearos\apps\base\File as File;

clearos_load_library('base/Engine');
clearos_load_library('base/File');

///////////////////////////////////////////////////////////////////////////////
// C L A S S
///////////////////////////////////////////////////////////////////////////////

/**
 * Generic REST client.
 *
 * @category   apps
 * @package    base
 * @subpackage libraries
 * @author     ClearFoundation <developer@clearfoundation.com>
 * @copyright  2018 ClearFoundation
 * @license    http://www.gnu.org/copyleft/lgpl.html GNU Lesser General Public License version 3 or later
 * @link       http://www.clearfoundation.com/docs/developer/apps/base/
 */

class Rest_Client extends Engine
{
    ///////////////////////////////////////////////////////////////////////////////
    // C O N S T A N T S
    ///////////////////////////////////////////////////////////////////////////////

    const HTTP_CODE_OK = 200;
    const HTTP_CODE_BAD_REQUEST = 400;
    const HTTP_CODE_UNAUTHORIZED = 401;
    const HTTP_CODE_FORBIDDEN = 403;
    const HTTP_CODE_NOT_FOUND = 404;
    const HTTP_CODE_VALIDATION_ERROR = 422;
    const HTTP_INTERNAL_SERVER_ERROR = 500;

    ///////////////////////////////////////////////////////////////////////////////
    // M E T H O D S
    ///////////////////////////////////////////////////////////////////////////////

    /**
     * Common entrypoint for making REST API requests.
     *
     * Exceptions should be handled by the caller.
     *
     * @param string $servers list of servers
     * @param string $route   API route
     * @param string $options options
     *
     * Options include:
     * - $type    request type - default: get
     * - $body    request body - default: none
     * - $verify  SSL verification - default: true
     *
     * @return array API response
     */

    public function request($servers, $route, $options = [])
    {
        clearos_profile(__METHOD__, __LINE__);

        // TODO: add support for multiple API servers
        $base_url = $servers[0] . $route;

        $type = isset($options['type']) ? $options['type'] : 'get';
        $body = isset($options['body']) ? $options['body'] : '';
        $verify = isset($options['verify']) ? $options['verify'] : TRUE;
        $username = empty($options['username']) ? TRUE : $options['username'];
        $password = empty($options['password']) ? TRUE : $options['password'];

        $client_options['base_url'] = $base_url;
        $client_options['defaults']['verify'] = $verify;

        if (!empty($username) && !empty($password))
            $client_options['defaults']['auth'] = [ $username, $password ];

        $client = new \GuzzleHttp\Client($client_options);

        try {
            if (empty($body)) {
                $response = $client->$type($route, [ 'exceptions' => false, 'stream' => false ]);
            } else {
                $request = $client->createRequest(
                    $type,
                    $route,
                    ['json' => $body, 'exceptions' => false, 'stream' => false ]
                );

                $response = $client->send($request);
            }

            $payload['status_code'] = 0;
            $payload['status_message'] = $response->getReasonPhrase();
            $payload['http_status_code'] = $response->getStatusCode();

            $body_handler = $response->getBody();
            $payload['body'] = $body_handler->read(1024000);
        } catch (\Exception $e) {
            $payload['status_code'] = -1;
            $payload['status_message'] = $e->getMessage();
            $payload['http_status_code'] = '';
            $payload['body'] = '';
        }

        return $payload;
    }
}
